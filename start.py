import sys
import os
import socket
import threading
import traceback
import time
from functools import wraps
from collections import namedtuple
from time import sleep
import ConfigParser

config = ConfigParser.ConfigParser()
config.read('config.cfg')

Server = namedtuple("Server", ["host", "port"])
User = namedtuple("User", ["nick", "ident", "realname"])
Msg = namedtuple("Msg", ["prefix", "command", "args"])

_buffer = {}
def readline(sock):
    global _buffer
    data = _buffer.get(sock, b"")
    while b"\n" not in data:
        new = sock.recv(2048)
        if not new:
            break
        data += new
    pos = data.find(b"\n")
    if pos == -1:
        _buffer[sock] = b""
        return data
    ret = data[:pos+1]
    _buffer[sock] = data[pos+1:]
    return ret

def parse(line):
    spl = []
    line = line.split(" :", 1)
    spl.extend(line[0].split(" "))
    try:
        spl.append(line[1])
    except IndexError:
        pass
    if spl[0].startswith(":"):
        prefix = spl.pop(0)
    else:
        prefix = ""
    return Msg(prefix, spl[0], spl[1:])
    
class PingReply(type):
    def __new__(cls, name, bases, dic):
        f = dic.get("doPrivmsg", lambda self, line: None)
        @wraps(f)
        def doPrivmsg(self, line):
            if line.args[1] == ".ping":
                self.send("PRIVMSG %s :Pong!" % line.args[0])
            if line.args[1] == ".die":
                self.send("QUIT :Module unloaded")
                sys.exit()
            if line.args[1] == ".servers":
                self.send("LINKS")
            if line.args[1] == ".stats":
                self.send("LUSERS")

            f(self, line)
        dic["doPrivmsg"] = doPrivmsg
        #def doNotice(self, line):
        #dic["doNotice"] = doNotice
        original_func = dic.get("doNotice", lambda self, line: None) # the old/original function or a "do nothing" function if the first doesn't exist
        def new_function(self, line):  
            if "NickServ" in line.prefix:
                if "This nickname is registered" in line.args[1]:
                    self.send("PRIVMSG %s :%s %s" % (config.get('nickauth', 'authbot'), config.get('nickauth', 'command'), config.get('nickauth', 'pass')))
            original_func(self, line)
        dic["doNotice"] = new_function
        return type.__new__(cls, name, bases, dic)


class IrcModule(object):
    def __init__(self, user, server):
        self.user = user
        self.server = server
        self.socket = None
    def feed(self, line):
        line = parse(line)
        cmd = line.command.title()
        name = "do%s" % cmd
        #print name
        if hasattr(self, name):
            getattr(self, name)(line)
    def send(self, text):
        if not isinstance(text, bytes):
            text = text.encode("utf-8")
        self.socket.send(text + b"\r\n")
    def run(self):
        self.socket = socket.socket()
        self.socket.connect(self.server)
        self.send("NICK %s" % self.user.nick)
        self.send("USER %s * * :%s" % (self.user.ident, self.user.realname))
        while 1:
            line = readline(self.socket)
            try:
                line = line.decode("utf-8")
            except UnicodeEncodeError:
                line = line.decode("ascii", "ignore")
            line = line.strip()
            print (line.encode("utf-8", "ignore"))
            self.feed(line)
    
    # Handlers
    def doPing(self, line):
        self.send("PONG %s" % line.args[0])
    def do001(self, line):
        self.send("OPER %s %s" % (config.get('oper', 'user'), config.get('oper', 'password')))
    def do381(self, line):
        self.send("JOIN %s,%s" % (config.get('channels', 'logchan'), config.get('channels', 'debugchan')))
    def do364(self, line):
        self.send("PRIVMSG %s Server: %s description: %s" % (config.get('channels', 'logchan'), line.args[1], line.args[3][2:]))
    def do251(self, line):
        raw = line.args[1].split()
        network_users = raw[5]
        network_servers = raw[8]
        global network_users
        global network_servers
    def do252(self, line):
        network_ops = line.args[1]
        global network_ops
    def do254(self, line):
        self.send("PRIVMSG %s This network has %s users, %s operators and %s channels on %s servers" % (config.get('channels', 'logchan') ,network_users, network_ops, line.args[1], network_servers))


# Subclass for the different bots
class MainBot(IrcModule):
    # Since it inherits all methods you only have to define the new/changed
    # ones (that's good because of DRY)
    __metaclass__ = PingReply
    prefix = "-"
    def doNotice(self, line):
        snotice = line.args[1].split()
        self.send("PRIVMSG %s %s" % (config.get('channels', 'debugchan'), snotice))
        
        #When a client connects.
        if "*** Notice -- Client connecting" in line.args[1]:
            if snotice[5] == "at":
                msg = "3Client: {0} connected to: {1} with hostname: {2}"
                self.send("PRIVMSG %s %s" % (config.get('channels', 'logchan'), msg.format(snotice[7], snotice[6].strip(":"), snotice[8].strip("()"))))
                if  snotice[8].strip("()") == "mibbot_sta@bot.search.mibbit.com": 
                    self.send("PRIVMSG %s 3[Client info] Mibbit bot, All it does is collecting info. (DO NOT TOUCH IT!)" % config.get('channels', 'logchan'))
            elif snotice[5] == "on":
                msg = "3Client: {0} connected to: bitchplease.iotairc.net with hostname: {1}"
                self.send("PRIVMSG %s %s" % (config.get('channels', 'logchan'), msg.format(snotice[8], snotice[9].strip("()"))))
                if  snotice[9].strip("()") == "mibbot_sta@bot.search.mibbit.com": 
                    self.send("PRIVMSG #m 3[Client info] Mibbit bot, All it does is collecting info. (DO NOT TOUCH IT!)")
                
        #When a client disocnnects.
        elif "*** Notice -- Client exiting" in line.args[1]:              
            msg = "4Client: {0} disconnected: {1}"  
            if snotice[4] == "exiting":
                user = snotice[7].split("!", 1)
                self.send("PRIVMSG %s %s" % (config.get('channels', 'logchan'), msg.format(user[0], " ".join(snotice[8:])[1:-1])))
                
            if snotice[4] == "exiting:":
                self.send("PRIVMSG %s %s" % (config.get('channels', 'logchan'), msg.format(snotice[5], " ".join(snotice[7:])[1:-1])))

            
      
# If all connect to the same server, you just have to define it once
ms_server = Server(host = config.get('connect', 'server'), port = config.getint('connect', 'port'))

# You don't need the keywords if you put them in the right order
ms_user = User(config.get('client', 'nick'), config.get('client', 'ident'), config.get('client', 'realname'))

main_bot = MainBot(ms_user, ms_server)
main_thread = threading.Thread(target = main_bot.run)
main_thread.start()

time.sleep(2)