from collections import namedtuple

buffer = {}

class IRCbot():
   def __init__(self):
      self.server = ""
      self.host = ""
      self.nick = ""
      self.ident = ""
      self.host = ""
      self.name = ""
      self.auth = ""
      
Msg = namedtuple("Msg", ["prefix", "command", "args"])
      
def parse(line):
     _parts = line.split(" :", 1)
     parts = (_parts[0].split(" "))
     try:
         parts.append(_parts[1])
     except:
         pass
     prefix = ""
     if parts[0].startswith(":"):
         prefix = parts[0][1:]
         parts = parts[1:]
     command = parts[0]
     args = parts[1:]
     return Msg(prefix, command, args)

def readline(sock):
   global buffer
   try:
      buffer[sock]
   except:
      buffer[sock] = ""
   while "\n" not in buffer[sock]:
      new = sock.recv(2**12)
      buffer[sock] += new
      if len(new) == 0:
         break
   splt = buffer[sock].split("\n", 1)
   try:
      buffer[sock] = splt[1]
   except:
      buffer[sock] = ""
   return splt[0].rstrip("\r\n")
   
def duration_human(seconds):
    seconds = long(round(seconds))
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    years, days = divmod(days, 365.242199)
 
    minutes = long(minutes)
    hours = long(hours)
    days = long(days)
    years = long(years)
 
    duration = []
    if years > 0:
        duration.append('%d years' % years + ''*(years != 1))
    else:
        if days > 0:
            duration.append('%d days' % days + ''*(days != 1))
        if hours > 0:
            duration.append('%d hours' % hours + ''*(hours != 1))
        if minutes > 0:
            duration.append('%d minuts' % minutes + ''*(minutes != 1))
        if seconds > 0:
            duration.append('%d seconds' % seconds + ''*(seconds != 1))
    return ' '.join(duration)
    
